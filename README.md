# Central Authentication Service

Central Authentication Service for manage Authentication and Authorization

## Setup Guidline

### With Docker

_TBD_

### Manual Instalations

To run this app on your pc please make sure that you have python > 3.8.1 installed.

#### 1. Install requirements

```shell script
    $ pip install -r requirements.txt # Install all listed dependencies in requirements.txt
```

#### 2. Set Environtment Variables

```shell script
    $ cp cas.env.example cas.env # Copy from example
    $ vim cas.env # Edit with your own config
    $ source cas.env # Export the value to your current terminal
```

#### 3. Run Postgres with Docker Compose (OPTIONAL)

Please make sure that you have installed docker-compose on your system

```shell script
    $ docker-compose up -d # Running postgres on docker
```

#### 4. Upgrade DB to Latest Version

```shell script
    $ flask db upgrade # Upgrade your DB
```

#### 5. Run The Flask Application

```shell script
    $ flask run
```

---

For further information please email to me@muhazrisofyan.com
