from http import HTTPStatus
from typing import Any

from lawcas.models.users import User
from lawcas.schemas.users import UserListResponseSchema

import time


class UserController:
    def __init__(self, **kwargs: dict) -> None:
        super().__init__()
        self.email = str(kwargs.get("email")).lower()

    def get_list(self) -> (HTTPStatus, Any):
        base_users_query = User.query.filter_by(is_deleted=False)
        total_user = base_users_query.count()
        users = base_users_query.all()

        time.sleep(2)

        data = UserListResponseSchema().dump(
            {
                "data": {"users": users, "total": total_user},
                "code": "200",
                "message": "Success get list users",
            }
        )

        return HTTPStatus.OK, data

    def create(self) -> (bool, Any):
        pass
