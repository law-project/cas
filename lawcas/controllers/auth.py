import base64
import hashlib
import os
from datetime import datetime, timedelta
from http import HTTPStatus
from typing import Any

import jwt
from flask import current_app as app

from lawcas.models.auth import UserToken
from lawcas.models.users import User
from lawcas.schemas.auth import RegisterResponseSchema, ValidateResponseSchema
from lawcas.schemas.commons import BaseErrorRensposeSchema, BaseResponseSchema


class AuthController:
    def __init__(self, **kwargs):
        super().__init__()
        self.email = str(kwargs.get("email")).lower()
        self.password = str(kwargs.get("password"))
        self.token = str(kwargs.get("token"))

    def register(self) -> (HTTPStatus, Any):
        user_exist = (
            User.query.filter_by(is_deleted=False, email=self.email).first() is not None
        )
        if user_exist:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": "Data tidak valid.",
                    "errors": {"email": ["Alamat email telah terdaftar."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        self.generate_password_and_salt()

        user = User(
            email=self.email, password=self.salted_password, salt=self.salt
        ).save()
        if not isinstance(user, User):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": f"Data tidak valid.",
                    "errors": {"email": ["Alamat email telah terdaftar."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        data = RegisterResponseSchema().dump(
            {
                "code": "201",
                "message": "Registrasi berhasil.",
                "data": {"token": self.salted_password},
            }
        )
        return HTTPStatus.CREATED, data

    def login(self) -> (HTTPStatus, Any):
        user = User.query.filter_by(is_deleted=False, email=self.email).first()
        if user is None:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "401",
                    "message": f"Data tidak valid.",
                    "errors": {"email": ["Data tidak valid."]},
                }
            )
            return HTTPStatus.UNAUTHORIZED, data

        self.user = user
        credentials_valid = self.check_password()
        if not credentials_valid:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "401",
                    "message": f"Data tidak valid.",
                    "errors": {"email": ["Data tidak valid."]},
                }
            )
            return HTTPStatus.UNAUTHORIZED, data

        self.generate_user_token()
        UserToken(user_id=self.user.id, token=self.token).save()
        data = RegisterResponseSchema().dump(
            {
                "code": "200",
                "message": "Login berhasil.",
                "data": {"token": self.token},
            }
        )
        return HTTPStatus.OK, data

    def logout(self) -> (HTTPStatus, Any):
        token = UserToken.query.filter_by(
            is_active=True, is_deleted=False, token=self.token
        ).first()
        if not token:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": f"Data tidak valid.",
                    "errors": {"email": ["Data tidak valid."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        token.is_active = False
        token.save()
        data = BaseResponseSchema().dump({"code": 200, "message": "Logout berhasil."})

        return HTTPStatus.OK, data

    def validate(self):
        token = UserToken.query.filter_by(
            is_active=True, is_deleted=False, token=self.token
        ).first()
        if not token:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": 401,
                    "message": f"Data tidak valid.",
                    "errors": {"email": ["Data tidak valid."]},
                }
            )
            return HTTPStatus.UNAUTHORIZED, data

        self.user_token = token

        try:
            self.decode_user_token()
        except Exception:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "401",
                    "message": f"Data tidak valid.",
                    "errors": {"token": ["Data tidak valid."]},
                }
            )
            return HTTPStatus.UNAUTHORIZED, data

        user = token.user
        data = ValidateResponseSchema().dump(
            {"code": 200, "message": "Validasi sukses.", "data": user}
        )
        return HTTPStatus.OK, data

    def generate_password_and_salt(self) -> None:
        salt = base64.b64encode(os.urandom(16)).decode("UTF-8")
        salted_password = hashlib.sha256(
            salt.encode("UTF-8") + self.password.encode("UTF-8")
        ).hexdigest()
        self.salt = salt
        self.salted_password = salted_password

    def check_password(self) -> bool:
        salted_password = hashlib.sha256(
            self.user.salt.encode("UTF-8") + self.password.encode("UTF-8")
        ).hexdigest()
        self.salted_password = salted_password
        return salted_password == self.user.password

    def generate_user_token(self) -> None:
        payload = {
            "sub": self.user.id,
            "iat": datetime.now(),
            "exp": datetime.now() + timedelta(hours=6),
        }

        token = jwt.encode(
            payload=payload, key=app.config.get("SECRET"), algorithm="HS256"
        )

        self.token = token.decode("UTF-8")

    def decode_user_token(self) -> dict:
        try:
            payload = jwt.decode(jwt=self.token, key=app.config.get("SECRET"))
        except jwt.ExpiredSignatureError:
            self.user_token.is_active = False
            self.user_token.save()
            return

        return payload
