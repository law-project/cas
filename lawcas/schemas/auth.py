from marshmallow import ValidationError, fields, post_load, validates
from marshmallow.validate import Email, Length

from lawcas import ma
from lawcas.schemas.users import UserSchema

from .commons import BaseResponseSchema


class RegisterValidationSchema(ma.Schema):
    email = fields.String(
        required=True, validate=Email(error="Alamat email tidak valid.")
    )
    password = fields.String(
        required=True, validate=Length(min=6, error="Password tidak valid.")
    )


class LoginValidationSchema(ma.Schema):
    email = fields.String(
        required=True, validate=Email(error="Alamat email tidak valid.")
    )
    password = fields.String(
        required=True, validate=Length(min=6, error="Password tidak valid.")
    )


class LogoutValidationSchema(ma.Schema):
    token = fields.String(required=True)

    @validates("token")
    def validate_token(self, value):
        message = "Token tidak valid."
        if not isinstance(value, str):
            raise ValidationError(message)

        value = str(value).split(" ")
        if len(value) != 2:
            raise ValidationError(message)

        if value[0] != "Bearer":
            raise ValidationError(message)

    @post_load()
    def split_token(self, data, **kwargs):
        data["token"] = data["token"].split(" ")[1]
        return data


class ValidateValidationSchema(LogoutValidationSchema):
    pass


class RegisterDataSchema(ma.Schema):
    token = fields.String(required=True)


class RegisterResponseSchema(BaseResponseSchema):
    data = fields.Nested(RegisterDataSchema)


class LoginDataSchema(ma.Schema):
    token = fields.String(required=True)


class LoginResponseSchema(BaseResponseSchema):
    data = fields.Nested(LoginDataSchema)


class ValidateResponseSchema(BaseResponseSchema):
    data = fields.Nested(UserSchema)
