from datetime import datetime

from marshmallow import fields

from lawcas import ma


class BaseResponseSchema(ma.Schema):
    code = fields.String()
    message = fields.String()
    timestamp = fields.DateTime(default=datetime.now())


class BaseErrorRensposeSchema(BaseResponseSchema):
    errors = fields.Raw()
