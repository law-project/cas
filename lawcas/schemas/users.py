from marshmallow import fields

from lawcas import ma
from lawcas.models.users import User

from .commons import BaseResponseSchema


class UserSchema(ma.ModelSchema):
    created_at = fields.DateTime("%A, %d %B %Y %H:%M:%S")
    updated_at = fields.DateTime("%A, %d %B %Y %H:%M:%S")

    class Meta:
        model = User
        exclude = (
            "password",
            "salt",
            "is_deleted",
            "deleted_at",
        )


class UserDataSchema(ma.Schema):
    users = fields.Nested(UserSchema(many=True))
    total = fields.Integer()


class UserListResponseSchema(BaseResponseSchema):
    data = fields.Nested(UserDataSchema())
