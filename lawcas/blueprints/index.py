"""
This file handle index's blueprints
"""
from flask import Blueprint, jsonify

index_blueprint = Blueprint("index", __name__, url_prefix="/")


@index_blueprint.route("", methods=("GET",))
def index() -> tuple:
    """
    This method response hello world in / path

    Returns:
        [tuple] -- [Json Response]
    """
    try:
        return jsonify({"message": "Hello, World!"}), 200
    except Exception as e:
        return jsonify({"message": f"ERROR: {e}"}), 500
