"""
This file handle users's blueprints
"""
from flask import Blueprint
from flask_restful import Api

from lawcas.resources.users import UsersResource

users_blueprint = Blueprint("Users", __name__, url_prefix="/users/")

users_resources = Api(users_blueprint)
users_resources.add_resource(UsersResource, "")
