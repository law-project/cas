"""
This file handle auth's blueprints
"""
from flask import Blueprint
from flask_restful import Api

from lawcas.resources.auth import (
    LoginResource,
    LogoutResource,
    RegisterResource,
    ValidateResource,
)

auth_blueprint = Blueprint("Auth", __name__, url_prefix="/auth/")

auth_resources = Api(auth_blueprint)
auth_resources.add_resource(RegisterResource, "register/")
auth_resources.add_resource(LoginResource, "login/")
auth_resources.add_resource(LogoutResource, "logout/")
auth_resources.add_resource(ValidateResource, "validate/")
