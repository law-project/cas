import json

from flask import Response
from flask_restful import Resource

# from http import HTTPStatus
# from lawcas.schemas.users import UserSchema
from lawcas.controllers.users import UserController


class UsersResource(Resource):
    def get(self):
        status, data = UserController().get_list()

        return Response(json.dumps(data), status=status, mimetype="application/json",)
