import json
from http import HTTPStatus

from flask import Response, request
from flask_restful import Resource
from marshmallow import ValidationError

from lawcas.controllers.auth import AuthController
from lawcas.schemas.auth import (
    LoginValidationSchema,
    LogoutValidationSchema,
    RegisterValidationSchema,
    ValidateValidationSchema,
)

# from http import HTTPStatus
from lawcas.schemas.commons import BaseErrorRensposeSchema


class RegisterResource(Resource):
    def post(self) -> Response:
        json_data = request.get_json(force=True)
        errors = RegisterValidationSchema().validate(json_data)
        if errors:
            data = BaseErrorRensposeSchema().dump(
                {"code": 422, "message": "Data tidak valid.", "errors": errors}
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNPROCESSABLE_ENTITY,
                mimetype="application/json",
            )

        status, data = AuthController(**json_data).register()

        return Response(json.dumps(data), status=status, mimetype="application/json",)


class LoginResource(Resource):
    def post(self) -> Response:
        json_data = request.get_json(force=True)
        errors = LoginValidationSchema().validate(json_data)
        if errors:
            data = BaseErrorRensposeSchema().dump(
                {"code": 422, "message": "Data tidak valid.", "errors": errors}
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNPROCESSABLE_ENTITY,
                mimetype="application/json",
            )

        status, data = AuthController(**json_data).login()

        return Response(json.dumps(data), status=status, mimetype="application/json",)


class LogoutResource(Resource):
    def post(self) -> Response:
        try:
            data = LogoutValidationSchema().load(
                {"token": request.headers.get("Authorization")}
            )
        except ValidationError as e:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": 422,
                    "message": "Data tidak valid.",
                    "errors": e.normalized_messages(),
                }
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNPROCESSABLE_ENTITY,
                mimetype="application/json",
            )

        status, data = AuthController(**data).logout()

        return Response(json.dumps(data), status=status, mimetype="application/json",)


class ValidateResource(Resource):
    def post(self) -> Response:
        try:
            data = ValidateValidationSchema().load(
                {"token": request.headers.get("Authorization")}
            )
        except ValidationError as e:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": 401,
                    "message": "Data tidak valid.",
                    "errors": e.normalized_messages(),
                }
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNAUTHORIZED,
                mimetype="application/json",
            )

        status, data = AuthController(**data).validate()

        return Response(json.dumps(data), status=status, mimetype="application/json",)
