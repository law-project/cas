"""
This file is the init file that provide method to create a modified flask app
"""
import http
import locale
import json

from flask import Flask, Response, request
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from typing import Any

from .tools.commons import discover_blueprints

try:
    locale.setlocale(locale.LC_TIME, "id_ID.utf8")
except locale.Error as e:
    print(f"LOCALE ERROR: {e}")
    pass


db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()


def resource_not_found(e: Any = "Not Found") -> Response:
    """
    This method is a error handler for http status Not Found

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Not Found"})

    Returns:
        [Response] -- [404 Response]
    """
    # return jsonify({"code": 404, "message": str(e)}), http.HTTPStatus.NOT_FOUND
    return Response(
        response=json.dumps({"code": 404, "message": str(e)}),
        status=http.HTTPStatus.NOT_FOUND,
        mimetype="application/json",
    )


def unauthorized(e: Any = "Unauthorized") -> Response:
    """
    This method is a error handler for http status Unauthorized

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Unauthorized"})

    Returns:
        [Response] -- [401 Response]
    """
    # return jsonify({"code": 401, "message": str(e)}), http.HTTPStatus.UNAUTHORIZED
    return Response(
        response=json.dumps({"code": 401, "message": str(e)}),
        status=http.HTTPStatus.UNAUTHORIZED,
        mimetype="application/json",
    )


def forbidden(e: Any = "Forbidden") -> Response:
    """
    This method is a error handler for http status Forbidden

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Forbidden"})

    Returns:
        [Response] -- [403 Response]
    """
    # return jsonify({"code": 403, "message": str(e)}), http.HTTPStatus.FORBIDDEN
    return Response(
        response=json.dumps({"code": 403, "message": str(e)}),
        status=http.HTTPStatus.FORBIDDEN,
        mimetype="application/json",
    )


def method_not_allowed(e: Any = "Method Not Allowed") -> Response:
    """
    This method is a error handler for http status Method Not Allowed

    Keyword Arguments:
        e {Any} -- [Exception or a message] (default: {"Method Not Allowed"})

    Returns:
        [Response] -- [403 Response]
    """
    # return jsonify({"code": 405, "message": str(e)}), http.HTTPStatus.METHOD_NOT_ALLOWED
    return Response(
        response=json.dumps({"code": 405, "message": str(e)}),
        status=http.HTTPStatus.METHOD_NOT_ALLOWED,
        mimetype="application/json",
    )


def create_app(test=False) -> Flask:
    """
    This method used to handle app creation and configure some objects

    Keyword Arguments:
        test {bool} -- [The test flag to distinguish between main app and test app] (default: {False})

    Returns:
        Flask -- [Configured Flask App]
    """
    app = Flask(__name__, instance_relative_config=False)

    if test:
        app.config.from_object("lawcas.config.TestConfig")
    else:
        app.config.from_object("lawcas.config.Config")

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    blueprints = discover_blueprints()

    with app.app_context():
        for blueprint in blueprints:
            try:
                app.register_blueprint(blueprint)
            except Exception as e:
                print(f"Failed to load blueprint {blueprint}: {e}")

        app.url_map.strict_slashes = True

        @app.before_request
        def handle_options_method() -> Any:
            """
            This method used to handle options method from front end

            Returns:
                Any -- [Return 204 if method is OPTIONS else return the original request]
            """
            if request.method == "OPTIONS":
                return Response(status=http.HTTPStatus.NO_CONTENT)

        app.register_error_handler(http.HTTPStatus.NOT_FOUND, resource_not_found)
        app.register_error_handler(http.HTTPStatus.UNAUTHORIZED, unauthorized)
        app.register_error_handler(http.HTTPStatus.FORBIDDEN, forbidden)
        app.register_error_handler(
            http.HTTPStatus.METHOD_NOT_ALLOWED, method_not_allowed
        )

        return app
