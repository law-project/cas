"""
    This file handle main app of flask that used by by uWSGI
"""
from lawcas import create_app

app = create_app()
