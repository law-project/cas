from datetime import datetime

import sqlalchemy as sa

from lawcas import db


class BaseModel(db.Model):
    __abstract__ = True

    id = sa.Column(sa.Integer(), primary_key=True)
    created_at = sa.Column(
        sa.DateTime(),
        default=sa.func.now(),
        server_default=sa.func.now(),
        nullable=False,
    )
    updated_at = sa.Column(
        sa.DateTime(),
        default=sa.func.now(),
        onupdate=sa.func.now(),
        nullable=False,
        server_default=sa.func.now(),
        server_onupdate=sa.func.now(),
    )
    is_deleted = sa.Column(sa.Boolean(), default=False, server_default="false")
    deleted_at = sa.Column(sa.DateTime(), default=None)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self

        except Exception:
            db.session.rollback()
            raise

    def add_flush(self):
        try:
            db.session.add(self)
            db.session.flush()
            return self

        except Exception:
            db.session.rollback()
            raise

    def delete(self):
        try:
            self.is_deleted = True
            self.deleted_at = datetime.now()

            db.session.add(self)
            db.session.commit()
            return True

        except Exception:
            db.session.rollback()
            raise
