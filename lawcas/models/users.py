import sqlalchemy as sa

from .commons import BaseModel


class User(BaseModel):
    __tablename__ = "users"
    email = sa.Column(sa.String(255), nullable=False, unique=True)
    password = sa.Column(sa.String(255), nullable=False)
    salt = sa.Column(sa.String(255), nullable=False)
