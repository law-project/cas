import sqlalchemy as sa

from .commons import BaseModel


class UserToken(BaseModel):
    __tablename__ = "user_tokens"
    user_id = sa.Column(sa.Integer(), sa.ForeignKey("users.id"), nullable=False)
    user = sa.orm.relationship("User")
    token = sa.Column(sa.String(500), nullable=False)
    is_active = sa.Column(
        sa.Boolean(), default=True, server_default="true", nullable=False
    )
