"""
    This file handle configuration data of the flask app
"""
import os

from env import EnvConfig

env = EnvConfig("LAWCAS")


class Config:
    """
        This class handle configuration for the main app
    """

    _basedir = os.path.abspath(os.path.dirname(__file__))

    SECRET = env.string(
        "SECRET", "GtZhXFojUOz1yiFYLIRA7aD8y1Op2bm1nLWmkseJSdNaIrfujqs2OAiiJCdEUSg9EN8"
    )

    FLASK_DEBUG = env.boolean("DEBUG", False)
    UTC_OFFSET = 7

    DB_NAME = env.string("DB_NAME", "lawcas")
    DB_USER = env.string("DB_USER", "lawcas")
    DB_PASS = env.string("DB_PASS", "password")
    DB_HOST = env.string("DB_HOST", "localhost")
    DB_PORT = env.string("DB_PORT", "5432")

    SQLALCHEMY_DATABASE_URI = (
        f"postgresql+psycopg2://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DATETIME_FORMAT = env.string("DATETIME_FORMAT", "%A, %d %B %Y %H:%M:%S")


class TestConfig(Config):
    """
        This class handle configuration for the test app
    """

    DB_TEST_PORT = env.string("DB_TEST_PORT", "5433")
    DB_TEST_PASS = env.string("DB_TEST_PASS", "password")
    DB_USER = Config.DB_USER
    DB_HOST = Config.DB_HOST
    DB_NAME = Config.DB_NAME

    SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{DB_USER}:{DB_TEST_PASS}@{DB_HOST}:{DB_TEST_PORT}/{DB_NAME}"
