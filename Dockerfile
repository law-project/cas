FROM alpine:latest
RUN apk add --no-cache python3-dev \
    && pip3 install --upgrade pip \
    && apk add --no-cache gcc musl-dev postgresql-libs postgresql-dev

WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt
EXPOSE 5000

# ENTRYPOINT [ "python3" ]
CMD [ "flask", "run", "--host=0.0.0.0" ]

# COPY . /app
# WORKDIR /app
# RUN \
#  apk add --no-cache postgresql-libs && \
#  apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
#  python3 -m pip install -r requirements.txt --no-cache-dir && \
#  apk --purge del .build-deps
# EXPOSE 5000
# ENTRYPOINT [ "python3" ]
# CMD [ "-m", "flask", "run", "--host", "0.0.0.0" ]
